<?php 
Class Common extends CI_Model {

	public function __construct() { 
		 parent::__construct(); 
		 $this->load->database(); 
	} 
	
	public function get_records_from_sql($sql)
	{
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function put_record_by_sql($sql)
	{
		return $this->db->query($sql);
	}
	public function insert_id()
	{
		return $this->db->insert_id();
	}
	
}

?>