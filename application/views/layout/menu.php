
<li class="nav-item">
  <a href="<?=base_url()?>welcome/blank" class="nav-link">
    <i class="nav-icon far fa-circle text-danger"></i>
    <p class="text">Blank Page</p>
  </a>
</li>
<li class="nav-item">
  <a href="<?=base_url()?>welcome/form" class="nav-link">
    <i class="nav-icon far fa-circle text-danger"></i>
    <p class="text">Form Page</p>
  </a>
</li>
<li class="nav-item">
  <a href="<?=base_url()?>welcome/table" class="nav-link">
    <i class="nav-icon far fa-circle text-danger"></i>
    <p class="text">Table Page</p>
  </a>
</li>